<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link href="../../assets/ico/favicon.ico" rel="shortcut icon">

    <title>Kişilik Testi Çıkış Sayfası</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="signin.css">



    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <form role="form" class="form-signin">
        <h3 class="form-signin-heading">Boşlukları doldurunuz.</h3>
        <input type="email" autofocus="" required="" placeholder="E-posta" class="form-control">
        <input type="isim" required="" placeholder="Ad" class="form-control">
        <input type="soyisim" required="" placeholder="Soyad" class="form-control">

        <button type="submit" class="btn btn-lg btn-primary btn-block">Gönder</button>
    </form>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


</body></html>